import connectToDevice from "../../api/DeviceService";
import { Alert } from "react-native";
import React, { useState } from "react";
import { View } from "react-native";
import { errorAlert } from "./errorHandling";

export const endpointRequest = async () => {
  const data = {
    entity_id: "78b88ef9-870d-48ce-ac90-0dba861cfa29",
    is_on: 1,
  };
  try {
    const result = await connectToDevice(data);
    return result.status;
  } catch (error) {
    const status = error.response.status;
    errorAlert(status);
    return status;
  }
};
