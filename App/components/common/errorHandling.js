import { Alert } from "react-native";
import React, { useState } from "react";
import { View } from "react-native";

export const errorAlert = (status) => {
  if (status == 401) {
    Alert.alert("Unauthorized request");
  } else if (status == 500) {
    Alert.alert("Connection error, please try again later");
  } else if (status == 403) {
    Alert.alert("Access denied");
  } else {
    Alert.alert("faild to connect");
  }
};
