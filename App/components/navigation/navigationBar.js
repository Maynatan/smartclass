import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Device } from "../Screens/ConnectScreen";
import { Home } from "../Screens/HomeScreen";
import { Lights } from "../Screens/LightScreen";
import { Ac } from "../Screens/AcScreen";

const DeviceStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();

export const DeviceStackScreen = () => (
  <DeviceStack.Navigator>
    <DeviceStack.Screen name="Device" component={Device} />
  </DeviceStack.Navigator>
);

export const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} />
    <HomeStack.Screen name="Lights" component={Lights} />
    <HomeStack.Screen name="Ac" component={Ac} />
  </HomeStack.Navigator>
);

export const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Device" component={DeviceStackScreen} />
    <Tabs.Screen name="Home" component={HomeStackScreen} />
  </Tabs.Navigator>
);
