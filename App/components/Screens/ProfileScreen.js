import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "../../Style/Style";
import { AuthContext } from "../../context";

//User Profile Screen
export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => signOut()}>
        <Text style={styles.btnLightText}>LOG OUT</Text>
      </TouchableOpacity>
    </View>
  );
};
