import React from "react";
import { View, Text } from "react-native";
import styles from '../../Style/Style';


//Splash Screen - for Login
export const Splash = () => (
  <View style={styles.container}> 
      <Text>Loading...</Text>
   </View>
  );
  