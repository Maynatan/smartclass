import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements/dist/buttons/Button";
import styles from "../../Style/Style";
import { endpointRequest } from "../common/endPoint";
import { CheckBox, Icon } from "react-native-elements";
import { RadioButton } from "react-native-paper";

//lights Control Screen
export const Lights = ({ route }) => {
  const [value, setValue] = React.useState("OFF");

  const selectHandlerLights = async (value) => {
    const lightsRequest = await endpointRequest();
    if (lightsRequest == 200) {
      setValue(value);
    }
  };

  return (
    <View style={styles.container}>
      <RadioButton.Group
        style={styles.btngroup}
        onValueChange={(value) => selectHandlerLights(value)}
        value={value}
      >
        <RadioButton.Item label="OFF" value="OFF" />
        <RadioButton.Item label="ALL" value="ALL" />
        <RadioButton.Item label="FRONT" value="FRONT" />
        <RadioButton.Item label="BACK" value="BACK" />
      </RadioButton.Group>
    </View>
  );
};
