import React from "react";
import { View, Text } from "react-native";
import { Switch } from "react-native-elements";
import { RadioButton } from "react-native-paper";
import styles from "../../Style/Style";
import { endpointRequest } from "../common/endPoint";

//Ac Control Screen
export const Ac = ({ route }) => {
  //for radiobutton group
  const [value, setValue] = React.useState("Cool");
  const [value1, setValue1] = React.useState("High");
  const [value2, setValue2] = React.useState("Off");
  const selectHandlerMood = async (value) => {
    const moodRequest = await endpointRequest();
    if (moodRequest == 200) {
      setValue(value);
    }
  };
  const selectHandlerFan = async (value) => {
    const fanRequest = await endpointRequest();
    if (fanRequest == 200) {
      setValue1(value);
    }
  };
  const selectHandlerPower = async (value) => {
    const fanRequest = await endpointRequest();
    if (fanRequest == 200) {
      setValue2(value);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerHome}>
        <Text style={styles.textAc}>Power</Text>
        <RadioButton.Group
          style={styles.btngroup}
          onValueChange={(value2) => selectHandlerPower(value2)}
          value={value2}
        >
          <RadioButton.Item label="Off" value="Off" />
          <RadioButton.Item label="On" value="On" />
        </RadioButton.Group>
      </View>

      <View style={styles.containerHome}>
        <Text style={styles.textAc}>Mood</Text>
        <RadioButton.Group
          onValueChange={(value) => selectHandlerMood(value)}
          value={value}
        >
          <RadioButton.Item label="Cool" value="Cool" />
          <RadioButton.Item label="Heat" value="Heat" />
          <RadioButton.Item label="Air" value="Air" />
        </RadioButton.Group>
      </View>

      <View style={styles.containerHome}>
        <Text style={styles.textAc}>Fan</Text>
        <RadioButton.Group
          onValueChange={(value1) => selectHandlerFan(value1)}
          value={value1}
        >
          <RadioButton.Item label="High" value="High" />
          <RadioButton.Item label="Medium" value="Medium" />
          <RadioButton.Item label="Low" value="Low" />
        </RadioButton.Group>
      </View>
    </View>
  );
};
