import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styles from "../../Style/Style";

//Connect Screen - for Sensors
export const Device = ({ navigation }) => (
  <View style={styles.container}>
    <TouchableOpacity>
      <Text style={styles.btnLightText}>Search a Device</Text>
    </TouchableOpacity>
  </View>
);
