import React, { useState } from "react";
import { View, Text, TouchableOpacity, TextInput } from "react-native";
import styles from "../../Style/Style";
import { AuthContext } from "../../context";
import authService from "../../api/AuthService";
import { Alert } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { Splash } from "./SplashScreen";
import { RootStackScreen } from "../navigation/navigationSignIn";
import { errorAlert } from "../common/errorHandling";

export const SignIn = ({ navigation }) => {
  const { signIn } = React.useContext(AuthContext);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Smart Class</Text>
      <Text style={styles.text}>Holon Institue of Technology</Text>
      <TextInput
        style={styles.input}
        placeholder="User Name"
        onChangeText={(username) => setUsername(username)}
        name="user"
        size={24}
        color="black"
        autoCapitalize="none"
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        onChangeText={(password) => setPassword(password)}
        secureTextEntry={true}
      />
      <TouchableOpacity
        style={styles.btnEnter}
        onPress={() => signIn(username, password)}
      >
        <Text style={styles.btnEnterText}>LOG IN</Text>
      </TouchableOpacity>
    </View>
  );
};

export const Auth = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userToken, setUserToken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: async (username, password) => {
        if (username == "") {
          Alert.alert("Please enter user name ");
        } else if (password == "") {
          Alert.alert("Please enter password");
        } else {
          const data = {
            user_name: username,
            pass: password,
          };
          try {
            const result = await authService(data);
            setUserToken(result.data[0].token);
          } catch (error) {
            errorAlert(error.response.status);
          }
        }
      },
      signOut: () => {
        setIsLoading(false);
        setUserToken(null);
      },
    };
  }, []);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);

  if (isLoading) {
    return <Splash />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStackScreen userToken={userToken} />
      </NavigationContainer>
    </AuthContext.Provider>
  );
};
