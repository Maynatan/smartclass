import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import { Button } from "react-native-elements";
import styles from "../../Style/Style";
import { MaterialIcons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";

//Home Screen
export const Home = ({ navigation }) => {
  return (
    <View style={styles.containerHome}>
      <TouchableOpacity
        style={styles.btnHome}
        onPress={() => navigation.push("Lights", { name: "Lights" })}
      >
        <Text style={styles.btnLightText}>
          {" "}
          Lights
          <MaterialIcons name="lightbulb-outline" size={60} color="black" />
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.btnHome}
        onPress={() => navigation.push("Ac", { name: "A/C" })}
      >
        <Text style={styles.btnLightText}>
          {" "}
          A/C
          <FontAwesome name="snowflake-o" size={60} color="black" />
        </Text>
      </TouchableOpacity>
    </View>
  );
};
