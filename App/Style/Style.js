import { StyleSheet } from "react-native";

//Style
export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
  },

  containerHome: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    flexDirection: "row",
  },

  welcome: {
    textAlign: "center",
    color: "#4c5c68",
    fontSize: 45,
  },

  text: {
    textAlign: "center",
    color: "#4c5c68",
    fontSize: 20,
    marginTop: 60,
  },

  textAc: {
    color: "#4c5c68",
    fontSize: 25,
    fontWeight: "bold",
    marginRight: 70,
    marginTop: 60,
    marginBottom: 90,
  },

  //sign in input
  input: {
    width: "80%",
    backgroundColor: "#f0efeb",
    height: 50,
    justifyContent: "center",
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10,
    margin: 40,
    borderRadius: 20,
  },

  //Sign in Button
  btnEnter: {
    width: "80%",
    backgroundColor: "#252422",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 60,
    marginBottom: 10,
    margin: 140,
    elevation: 5,
  },

  btnLight: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },

  btnHome: {
    height: 80,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 10,
    margin: 30,
  },

  //Text Button
  btnEnterText: {
    color: "#ffffff",
    fontWeight: "700",
    fontWeight: "bold",
  },

  btnLightText: {
    color: "#4c5c68",
    fontSize: 20,
  },
});
