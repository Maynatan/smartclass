import axios from "axios";

let instanceAxios = axios.create({
  baseURL: process.env.BASE_URL,
  headers: {
    //"Access-Control-Allow-Original": "*",
    //"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
    //"Access-Control-Allow-Headers":
    //"Origin, X-Request-With, Content-Type, Accept, Authorization",
    Accept: "application/json",
    "Content-type": "application/json",
  },
});

export default instanceAxios;
