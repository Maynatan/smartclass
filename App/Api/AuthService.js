import instanceAxios from "./http-common";

//const url = '/login';
const url = "https://www.hitprojectscenter.com/LocationCounterAPI/login";

const authService = (data) => {
  return instanceAxios.post(url, data);
};
export default authService;
