import instanceAxios from "./http-common";

//const url = '/command';
const url = "https://www.hitprojectscenter.com/LocationCounterAPI/command";

const connectToDevice = (data) => {
  return instanceAxios.post(url, data);
};
export default connectToDevice;
