import { NavigationContainer } from "@react-navigation/native";
import { LogBox } from "react-native";
import React, { useState } from "react";
import { AuthContext } from "./context";
import { Splash } from "./components/Screens/SplashScreen";
import { RootStackScreen } from "./components/navigation/navigationSignIn";
import { Alert } from "react-native";
import { Auth } from "./components/Screens/LoginScreen";

//Hide Warnings From Screen
LogBox.ignoreAllLogs();

export default () => {
  return Auth();
};
